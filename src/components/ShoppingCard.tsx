import { Offcanvas, Stack } from "react-bootstrap";
import { useShoppingCard } from "../context/ShoppingCardContext";
import CardItem from "./CardItem";
import { formatCurrency } from "../utilities/formatCurrency";
type shoppingCardProps = {
  isOpen: boolean;
};
import storeItems from "../data/item.json";

const ShoppingCard = ({ isOpen }: shoppingCardProps) => {
  const { closeCard, cardItems } = useShoppingCard();
  return (
    <>
      <Offcanvas show={isOpen} onHide={closeCard} placement="end">
        <Offcanvas.Header closeButton>
          <Offcanvas.Title>Card</Offcanvas.Title>
        </Offcanvas.Header>
        <Offcanvas.Body>
          <Stack gap={3}>
            {cardItems.map((item) => (
              <CardItem key={item.id} {...item} />
            ))}
            <div className="ms-auto fs-5 fw-bold">
              Total:
              {formatCurrency(
                cardItems.reduce((total, CardItem) => {
                  const item = storeItems.find((i) => i.id === CardItem.id);
                  return total + (item?.price || 0) * CardItem.quantity;
                }, 0)
              )}
            </div>
          </Stack>
        </Offcanvas.Body>
      </Offcanvas>
    </>
  );
};

export default ShoppingCard;
