import { Button, Stack } from "react-bootstrap";
import { useShoppingCard } from "../context/ShoppingCardContext";
import storeItems from "../data/item.json";
import { formatCurrency } from "../utilities/formatCurrency";

type cardItemProps = {
  id: number;
  quantity: number;
};

const CardItem = ({ id, quantity }: cardItemProps) => {
  const { removeFromCard } = useShoppingCard();
  const item = storeItems.find((i) => i.id === id);
  if (item == null) return null;

  return (
    <Stack direction="horizontal" gap={2} className="d-flex align-items-center">
      <img
        src={item.imgUrl}
        alt="Item img"
        style={{ width: "100px", height: "75px", objectFit: "cover" }}
      />
      <div className="me-auto">
        <div className="text-capitalize">
          {item.name}
          {quantity > 1 && (
            <span className="text-muted" style={{ fontSize: ".65rem" }}>
              x{quantity}
            </span>
          )}
        </div>
        <div className="text-muted" style={{ fontSize: ".75rem" }}>
          {formatCurrency(item.price)}
        </div>
      </div>
      <div>{formatCurrency(item.price * quantity)}</div>
      <Button
        variant="outline-danger"
        size="sm"
        onClick={() => removeFromCard(item.id)}
      >
        &times;
      </Button>
    </Stack>
  );
};

export default CardItem;
