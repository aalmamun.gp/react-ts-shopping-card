import { Button, Card } from "react-bootstrap";
import { formatCurrency } from "../utilities/formatCurrency";
import { useShoppingCard } from "../context/ShoppingCardContext";

type StoreItemProps = {
  id: number;
  name: string;
  price: number;
  imgUrl: string;
};
export function StoreItem({ id, name, price, imgUrl }: StoreItemProps) {
  const {
    getItemQuentity,
    increaseCardQuentity,
    decreaseCardQuentity,
    removeFromCard,
  } = useShoppingCard();
  const quantity = getItemQuentity(id);
  return (
    <Card className="h-100">
      <Card.Img
        variant="top"
        src={imgUrl}
        height="200px"
        style={{ objectFit: "cover" }}
      />
      <Card.Body>
        <Card.Title className="d-flex justify-content-between align-items-baseline">
          <span className="text-capitalize fs-3">{name}</span>
          <p className="text-muted">{formatCurrency(price)}</p>
        </Card.Title>

        <div className="mt-auto">
          {quantity === 0 ? (
            <Button
              onClick={() => increaseCardQuentity(id)}
              type="button"
              className="text-capitalize w-100"
            >
              +add to card
            </Button>
          ) : (
            <div
              className="d-flex align-items-center flex-column"
              style={{ gap: ".5rem" }}
            >
              <div
                style={{ gap: ".5rem" }}
                className="d-flex justify-content-center align-items-center"
              >
                <Button
                  onClick={() => decreaseCardQuentity(id)}
                  type="button"
                  className=""
                >
                  -
                </Button>
                <div>
                  <span className="fs-4">{quantity}</span> in card
                </div>
                <Button
                  onClick={() => increaseCardQuentity(id)}
                  type="button"
                  className=""
                >
                  +
                </Button>
              </div>
              <Button
                onClick={() => removeFromCard(id)}
                type="button"
                variant="danger"
                size="sm"
              >
                Remove
              </Button>
            </div>
          )}
        </div>
      </Card.Body>
    </Card>
  );
}
