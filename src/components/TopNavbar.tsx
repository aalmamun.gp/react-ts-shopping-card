import { Navbar, Nav, Container, Button } from "react-bootstrap";
import { NavLink } from "react-router-dom";
import { useShoppingCard } from "../context/ShoppingCardContext";
const TopNavbar = () => {
  const { openCard, cardQuantity } = useShoppingCard();

  return (
    <Navbar sticky="top" className="bg-white shadow-sm mb-3">
      <Container>
        <Nav className="me-auto">
          <Nav.Link to="/" as={NavLink}>
            Home
          </Nav.Link>
          <Nav.Link to="/store" as={NavLink}>
            Store
          </Nav.Link>
          <Nav.Link to="/about" as={NavLink}>
            About
          </Nav.Link>
        </Nav>
        {cardQuantity > 0 && (
          <Button
            onClick={openCard}
            style={{ width: "3rem", height: "3rem" }}
            type="button"
            variant="outline-primary"
            className="rounded-circle position-relative"
          >
            <span>SP</span>

            <div
              style={{
                width: "1.5rem",
                height: "1.5rem",
                position: "absolute",
                right: 0,
                bottom: 0,
                transform: "translate(25%, 25%)",
              }}
              className="text-white d-flex justify-content-center align-items-center rounded-circle bg-danger"
            >
              {cardQuantity}
            </div>
          </Button>
        )}
      </Container>
    </Navbar>
  );
};

export default TopNavbar;
