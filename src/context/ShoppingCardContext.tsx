import { useState } from "react";
import { useContext, createContext, ReactNode } from "react";
import ShoppingCard from "../components/ShoppingCard";
import { useLocalStorage } from "../hooks/useLocalStorage";

type ShoppingCardProviderProps = {
  children: ReactNode;
};
type CardItem = {
  id: number;
  quantity: number;
};

type ShoppingCardContext = {
  openCard: () => void;
  closeCard: () => void;
  getItemQuentity: (id: number) => number;
  increaseCardQuentity: (id: number) => void;
  decreaseCardQuentity: (id: number) => void;
  removeFromCard: (id: number) => void;
  cardQuantity: number;
  cardItems: CardItem[];
};

const ShoppingCardContext = createContext({} as ShoppingCardContext);

export function useShoppingCard() {
  return useContext(ShoppingCardContext);
}

export function ShoppingCardProvider({ children }: ShoppingCardProviderProps) {
  
  const [isOpen, setIsOpen] = useState(false);

  const [cardItems, setCardItems] = useLocalStorage<CardItem[]>(
    "shopping-card",
    []
  );

  const cardQuantity = cardItems.reduce(
    (quantity, item) => item.quantity + quantity,
    0
  );

  const openCard = () => setIsOpen(true);
  const closeCard = () => setIsOpen(false);

  function getItemQuentity(id: number) {
    return cardItems.find((item) => item.id === id)?.quantity || 0;
  }

  function increaseCardQuentity(id: number) {
    setCardItems((currItem) => {
      if (currItem.find((item) => item.id === id) == null) {
        return [...currItem, { id, quantity: 1 }];
      } else {
        return currItem.map((item) => {
          if (item.id === id) {
            return { ...item, quantity: item.quantity + 1 };
          } else {
            return item;
          }
        });
      }
    });
  }

  function decreaseCardQuentity(id: number) {
    setCardItems((currItem) => {
      if (currItem.find((item) => item.id === id)?.quantity === 1) {
        return currItem.filter((item) => item.id !== id);
      } else {
        return currItem.map((item) => {
          if (item.id === id) {
            return { ...item, quantity: item.quantity - 1 };
          } else {
            return item;
          }
        });
      }
    });
  }

  function removeFromCard(id: number) {
    setCardItems((currItem) => {
      return currItem.filter((item) => item.id !== id);
    });
  }

  return (
    <ShoppingCardContext.Provider
      value={{
        getItemQuentity,
        increaseCardQuentity,
        decreaseCardQuentity,
        removeFromCard,
        openCard,
        closeCard,
        cardItems,
        cardQuantity,
      }}
    >
      {children}

      <ShoppingCard isOpen={isOpen} />
    </ShoppingCardContext.Provider>
  );
}
