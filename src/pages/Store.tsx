import storeItem from "../data/item.json";
import { Row, Col } from "react-bootstrap";
import { StoreItem } from "../components/StoreItem";
const Store = () => {
  return (
    <>
      <h1>Store page</h1>
      <Row xs={1} md={2} lg={4} className="g-3">
        {storeItem.map((item) => (
          <Col key={item.id}>
            <StoreItem {...item} />
          </Col>
        ))}
      </Row>
    </>
  );
};

export default Store;
